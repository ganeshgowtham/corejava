package com.gitlab.gowth.model;

import java.util.Objects;

public class Person {
    @Override
    public String toString() {
        return "Person{" +
                "fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", salary=" + salary +
                '}';
    }

    private String fname;
    private String lname;
    private long salary;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return salary == person.salary &&
                Objects.equals(fname, person.fname) &&
                Objects.equals(lname, person.lname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fname, lname, salary);
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public Person(String fname, String lname, long salary) {
        this.fname = fname;
        this.lname = lname;
        this.salary = salary;
    }
}
