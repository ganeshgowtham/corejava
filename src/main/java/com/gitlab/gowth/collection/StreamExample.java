package com.gitlab.gowth.collection;

import com.gitlab.gowth.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StreamExample {
    public static void main(String[] args) {
        List<Person> list = new ArrayList<>();
        list.add(new Person("a1", "a2", 1));
        list.add(new Person("b1", "b2", 2));
        list.add(new Person("c1", "c2", 3));
        list.add(new Person("d1", "d2", 4));
        list.add(new Person("e1", "e2", 10));
        Predicate<Person> p1 = s -> s.getFname().startsWith("A");
        /*list.stream().filter(data -> data.getSalary() > 3).map(x -> {
            return x.getFname();
        }).forEach(d -> System.out.println(d));*/

        System.out.println(list.stream().collect(Collectors.averagingLong(s -> s.getSalary())));

    }
}
